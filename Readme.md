# Lamentable REST project

## Scenario:

A new colleague has been given a task to create a microservice.  
They were required to build two REST endpoints with the following acceptance criteria:

1. A service to allow the upload of a single file by a REST client.
2. The upload service will save the file (a later change will implement a download facility)
3. A record will be kept of all file uploads identifying:
    - the name of the user who uploaded the file
    - the name of the file
    - the time it was uploaded
4. Upload records will be available to retrieve using the REST client
5. Automated tests must be provided to prove it works.

You have been asked to review the work and provide feedback.

## Task
Please spend no more than 2 hours reviewing the code, making comments and perhaps 
make some changes of your own to show some improvements in the most important areas.
This is not a test of diplomacy; there are no feelings to protect, so tell us exactly what you think.

We would appreciate feedback before your interview to give us time to read your comments.
We are not mandating a format for this, however you might find it useful 
to fork the repository 
to your own account if you have one.  Please do not make the code public.
